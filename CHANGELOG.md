# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.1.0] - 2020-02-01
### Added
- Main template modules: `core/`, `pages/`, `ui/`

[Unreleased]: https://gitlab.com/gcazef/nebular-light-starter
[0.1.0]: https://gitlab.com/gcazef/nebular-light-starter/-/tags/v0.1.0