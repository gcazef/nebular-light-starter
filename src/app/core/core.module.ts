import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


/**
 * This modules contains shared logic for all project.
 */
@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
