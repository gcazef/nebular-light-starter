import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { User } from '../models/user.model';


/**
 * This is a mock authentication service.
 * Modify login, logout and canActivate implementations to make it suit your needs.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  private _user: User;

  constructor(
    private router: Router
  ) { }

  canActivate(): boolean {
    if (!this._user) {
      this.router.navigateByUrl('/login');
      return false;
    }
    return true;
  }

  getUser(): User {
    return this._user;
  }

  /**
   * Login as user
   * @param user user credentials
   */
  login(user: User) {
    this._user = user;
    this.router.navigateByUrl('');
  }

  logout() {
    this._user = null;
    this.router.navigateByUrl('/login');
  }
}
