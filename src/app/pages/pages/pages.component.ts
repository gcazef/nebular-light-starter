import { Component } from '@angular/core';
import { MENU_ITEMS } from './pages.menu';


/**
 * Page component that implements main-layout.
 */
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent {

  pages = MENU_ITEMS;

}
