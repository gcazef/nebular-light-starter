import { Component } from '@angular/core';

/**
 * Very basic two-colums view based on bootstrap classes.
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {}
