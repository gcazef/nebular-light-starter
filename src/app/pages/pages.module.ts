import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component';
import { UiModule } from '../ui/ui.module';
import { NbMenuModule, NbCardModule, NbInputModule, NbButtonModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';


/**
 * This module contains components that represent application pages.
 */
@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NbMenuModule,
    NbCardModule,
    NbInputModule,
    NbButtonModule,
    PagesRoutingModule,
    UiModule
  ]
})
export class PagesModule { }
