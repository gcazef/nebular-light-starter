import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { NbLayoutModule, NbSidebarModule, NbIconModule, NbActionsModule, NbContextMenuModule, NbUserModule } from '@nebular/theme';
import { CenteredLayoutComponent } from './layouts/centered-layout/centered-layout.component';


/**
 * Module containing reusable layouts and UI components
 */
@NgModule({
  declarations: [
    HeaderComponent,
    MainLayoutComponent,
    CenteredLayoutComponent
  ],
  imports: [
    CommonModule,
    NbLayoutModule,
    NbSidebarModule,
    NbIconModule,
    NbActionsModule,
    NbContextMenuModule,
    NbUserModule
  ],
  exports: [
    MainLayoutComponent,
    CenteredLayoutComponent
  ]
})
export class UiModule { }
