import { Component } from '@angular/core';

/**
 * A layout with content centered on the page.
 * Useful for login or register pages.
 */
@Component({
  selector: 'app-centered-layout',
  templateUrl: './centered-layout.component.html',
  styleUrls: ['./centered-layout.component.scss']
})
export class CenteredLayoutComponent { }
