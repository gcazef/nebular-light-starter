import { Component } from '@angular/core';

/**
 * Layout with a header and a collapsable side menu.
 */
@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent {}
