# NebularLightStarter

A very simple Angular 8 template built with [Nebular 4.6.0](https://akveo.github.io/nebular/) and [Bootstrap 4](https://getbootstrap.com/).  

Licensed under the MIT License. See `LICENSE.txt`.

## Contents

`core/`: shared logic (contains a mock authentication service)  
`pages/`: application pages and routing  
`ui/`: reusable UI components  

## How to use

First, download code archive or fork this repository. You can then customize the template to use it for your own projects.

### Change project name

WARNING: remove node_modules folder to avoid renaming what is inside.  
You can change the name using ```sed -i 's/nebular-light-starter/newName/g' *```, where newName is the name of your project.  
Then, reinstall dependencies with `npm i`.

### Modify authentication logic

Setup your authentication in `core/services/auth.service.ts`.

### Create pages

Generate a new component under `pages/` module. Add a new route in `pages-routing.module.ts`.  
If the route belongs to `PagesComponent` children, it will use the same layout with header and side menu.  
Finally, if you want to put a link to this page in your side menu, edit the file `pages/pages/pages.menu.ts`.  
You can search for icons [here](https://akveo.github.io/eva-icons/#/).
